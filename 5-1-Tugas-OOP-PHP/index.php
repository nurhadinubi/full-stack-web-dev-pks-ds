<?php 
  trait Hewan {
    public $nama;
    public $darah;
    public $jumlahKaki;
    public $keahlian;

    abstract function atraksi();
   }

  trait Fight {
    
    public $attackPower;
    public $defencePower;

    abstract function serang();
    abstract function diserang();
  }

  class Elang  {
   use Hewan;
   use Fight;
  
   public function __construct($nama,$darah=10,$jumlahKaki,$keahlian,$attackPower=0,$deffencePower=0 )
    {
      $this->nama = $nama;
      $this->darah = $darah;
      $this->jumlahKaki = $jumlahKaki;
      $this->keahlian= $keahlian;
      $this->attackPower=$attackPower;
      $this->deffencePower = $deffencePower ;
    }
  
    function atraksi(){
      return $this->nama ." dapat ". $this->keahlian;
    }

    function serang($hewan){
      return "<hr>". $this->nama. " Sedang menyerang ".$hewan->nama;
    }
    function diserang($hewan){
      $output = "<hr>". $this->nama. " Sedang diserang ". $hewan->nama;
      $sisaDarah = $this->darah - ($hewan->attackPower / $this->deffencePower);
      return $output ."<br /> Sisa Darah Sekarang : ".$sisaDarah;
    }

    public function getInfoHewan(){
      $info = "Jenis : " . get_class($this);
      $info .= "<br/>Nama : ". $this->nama;
      $info .= "<br/>darah : ". $this->darah;
      $info .= "<br/>jumlahKaki : ". $this->jumlahKaki;
      $info .= "<br/>keahilan : ".  $this->keahlian;
      $info .= "<br/>attackPower : ". $this->attackPower;
      $info .= "<br/>deffencePower : ". $this->deffencePower;
      return $info;
    }

  }
  // Make Class Harimau
  class Harimau  {
    use Hewan;
    use Fight;
   
    public function __construct($nama,$darah=10,$jumlahKaki,$keahlian,$attackPower=1,$deffencePower=1 )
     {
       $this->nama = $nama;
       $this->darah = $darah;
       $this->jumlahKaki = $jumlahKaki;
       $this->keahlian= $keahlian;
       $this->attackPower=$attackPower;
       $this->deffencePower = $deffencePower ;
     }
   
     function atraksi(){
       return $this->nama ." dapat ". $this->keahlian;
     }
 
     function serang($hewan){
      return "<hr>". $this->nama. " Sedang menyerang ".$hewan->nama;
     }
     function diserang($hewan){
     $output = "<hr>". $this->nama. " Sedang diserang ". $hewan->nama;
     $sisaDarah = $this->darah - ($hewan->attackPower / $this->deffencePower);

     return $output ."<br /> Sisa Darah Sekarang : ".$sisaDarah;
     }
 
     public function getInfoHewan(){
       $info = "Jenis :" . get_class($this);
       $info .= "<br/>Nama : ". $this->nama;
       $info .= "<br/>darah : ". $this->darah;
       $info .= "<br/>jumlahKaki : ". $this->jumlahKaki;
       $info .= "<br/>keahilan : ".  $this->keahlian;
       $info .= "<br/>attackPower : ". $this->attackPower;
       $info .= "<br/>deffencePower : ". $this->deffencePower;
       return $info;
     }
 
   }

  $elang1 = new Elang("Elang 1",68,2,"Terbang Tinggi",10,5);
  $harimau1 = new Harimau("Harimau 1",78,4,"Lari Cepat",7,8);
  echo($harimau1->getInfoHewan());
  echo($harimau1->serang($elang1));
  echo($harimau1->diserang($elang1));

  echo("<hr/>");
  echo($elang1->getInfoHewan());
  echo($elang1->serang($harimau1));
  echo($elang1->diserang($harimau1));
?>