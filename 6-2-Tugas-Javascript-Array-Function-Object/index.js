// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
console.info("Nomor 1")
daftarHewan.forEach(item=>{
  console.log(item);
})


// Soal 2
const introduce = data =>{
  return `Nama saya ${data.name}, umur saya ${data.age} tahun, alamat saya di ${data.address}, dan saya punya hobby yaitu ${data.hobby}!`
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" };
var perkenalan = introduce(data);
console.log('\nNomor 2.')
console.log(perkenalan);


// Soal 3 
const hitung_huruf_vokal = str =>{
  var m = str.match(/[aeiou]/gi);
  return m === null ? 0 : m.length;
}
var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log("\nNomor 3.")
console.log(hitung_1 , hitung_2);



// Soal 4

console.log('\nNomor 4')
const hitung = angka =>{
  return (angka-1)*2;
}
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8
