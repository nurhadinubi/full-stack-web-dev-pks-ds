/*
  Soal 1
  buatlah fungsi menggunakan arrow function luas dan keliling persegi panjang dengan arrow function lalu gunakan let atau const di dalam soal ini
*/
// Jawaban Soal 1. fungsi luas & keliling persegi panjang
console.log('Soal No 1')
const luasPersegiPanjang = (panjang,lebar)=>{
  let luas = 0;
  luas = panjang*lebar;
  return luas;
}

const kelilingPersegiPanjang = (panjang,lebar)=>{
  let keliling = 0;
  keliling = (panjang+lebar)*2;
  return keliling;
}

console.log("Luas Persegi Panjang 6 x 4 adalah : ",luasPersegiPanjang(6,4))
console.log("Keliling Persegi Panjang 6 x 4 adalah : ",kelilingPersegiPanjang(6,4))

/*
  Soal 2
  buatlah fungsi menggunakan arrow function luas dan keliling persegi panjang dengan arrow function lalu gunakan let atau const di dalam soal ini
*/
// const newFunction = function literal(firstName, lastName){
//   return {
//     firstName: firstName,
//     lastName: lastName,
//     fullName: function(){
//       console.log(firstName + " " + lastName)
//     }
//   }
// }

// Jawaban No 2
console.log('\nSoal No 2')
const newFunction = (firstName,lastName)=>{
  return {
    firstName,
    lastName,
    fullName:() => console.log(`${firstName} ${lastName}`)
  }
}

newFunction("William", "Imoh").fullName();
console.log("First Name :", newFunction("William", "Imoh").firstName);
console.log("Last Name :", newFunction("William", "Imoh").lastName);


// Soal No 3 Destructuring Object
console.log('\nSoal No 3')
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};

const {firstName,lastName,address,hobby} = newObject;
console.log(firstName, lastName, address, hobby);


// Soal No 4. Kombinasikan dua array berikut menggunakan array spreading ES6
console.log('\nSoal No 4');
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];

const combined = [...west,...east];
console.log(combined);


//Jawaban  Soal No 5 Template literals
console.log('\nSoal No 5')
const planet = "earth" ;
const view = "glass" ;
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet ;
const templateLiteral = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;

console.log("Before : ",before);
console.log("After : ",templateLiteral);