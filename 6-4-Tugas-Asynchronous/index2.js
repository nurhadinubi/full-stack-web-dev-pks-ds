// di index.js
var readBooksPromise = require('./promise.js');
 
var books = [
  {name: 'LOTR', timeSpent: 3000}, 
  {name: 'Fidas', timeSpent: 2000}, 
  {name: 'Kalkulus', timeSpent: 4000},
  {name: 'komik', timeSpent: 1000}
];

readBooksPromise(10000,books[0])
.then(time=>readBooksPromise(time,books[1]))
.then(time2=>readBooksPromise(time2,books[2]))
.then(time3=>readBooksPromise(time3,books[3]))
.catch(err=>console.log(err))
