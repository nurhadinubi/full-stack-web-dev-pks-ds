export const LoginComponent = {
  data(){
    return{
      username:'',
      users:
        {
          username:'',
          login :false,
          role :'admin'
        }
      
    } 
  },
  template: `
      <div >
          <h1>Login</h1>
          <input name='username' id='username' placeholder='username' v-model='username' type='text' />
          <input name='password' id='password' placeholder='password' type='password'/>
          <button @click="doLogin(username)">Login</button>
    </div>`,

  methods:{
    doLogin:function(user){
      this.users.username = user;
      this.users.login = true;
      console.log(this.users);
    }
  }
 
}