<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{    
    protected $newComment;

    public function __construct()
    {
        $this->newComment=  new Comment();;
    }
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table comments
        $comments = $this->newComment::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Comment',
            'data'    => $comments  
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find post by ID
       
        $comment = $this->newComment->findOrfail($id);
       

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Komentar',
            'data'    => $comment 
        ], 200);

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'postId' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $comment = $this->newComment::create([
            'content'     => $request->content,
            'post_id'   => $request->postId
        ]);

        //success save to database
        if($comment) {

            return response()->json([
                'success' => true,
                'message' => 'Komentar Created',
                'data'    => $comment  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Post Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $comment
     * @return void
     */
    public function update(Request $request, Comment $comment)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'postId' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $comment = $this->newComment::findOrFail($comment->id);

        if($comment) {

            //update post
            $comment->update([
                'content'     => $request->content,
                'post_id'   => $request->postId
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Komentar Updated',
                'data'    => $comment  
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Komentar Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $comment = $this->newComment::findOrfail($id);

        if($comment) {

            //delete post
            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Komentar Deleted',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Komentar Not Found',
        ], 404);
    }
}