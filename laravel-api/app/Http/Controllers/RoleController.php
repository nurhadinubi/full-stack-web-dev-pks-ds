<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::latest()->get();

        if($roles){

            //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'List Data Roles berhasil ditampilkan',
                'data'    => $roles  
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'There is no role in database',
        ], 404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //set validation
         $validator = Validator::make($request->all(), [
            'name'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $role = Role::create([
            'name'     => $request->name
            
        ]);

        //success save to database
        if($role) {

            return response()->json([
                'success' => true,
                'message' => 'New Role Created',
                'data'    => $role  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Role Failed to Save',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  mixed  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
        $role = Role::find($id);
        
        if($role){
            //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'Detail Data Role',
                'data'    => $role 
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Role dengan id : '.$id.' tidak ditemukan',
            
        ], 404);
       
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         //set validation
         $validator = Validator::make($request->all(), [
            'name'   => 'required',
            
        ]);
        
       
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

     
        $role = role::find($id);

        if($role) {

            //update role
            $role->update([
                'name'     => $request->name,
               
            ]);

            return response()->json([
                'success' => true,
                'message' => 'role Updated',
                'data'    => $role  
            ], 200);

        }

      
        return response()->json([
            'success' => false,
            'message' => 'Role Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         //find post by ID
         $role = Role::findOrfail($id);

         if($role) {
 
             //delete role
             $role->delete();
 
             return response()->json([
                 'success' => true,
                 'message' => 'role Deleted',
             ], 200);
 
         }
 
         //data role not found
         return response()->json([
             'success' => false,
             'message' => 'role Not Found',
         ], 404);
    }
}
