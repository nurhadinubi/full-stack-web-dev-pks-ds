<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class OtpCode extends Model
{
    protected $table = "otp_codes";
    protected $fillable = ['otp','user_id','valid_until'];
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();
        static::creating(function($model){
            if(empty($model->id)){
                $model->id = Str::uuid();
                
            }
        });
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
