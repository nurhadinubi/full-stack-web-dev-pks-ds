<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class User extends Model
{
    protected $table = 'user';
    protected $fillable = ['name','email','role_id','password','email_verified_at','username'];
    protected $keyType = 'string';
    public $incrementing = false;
    
    protected static function boot()
    {
        parent::boot();
        static::creating(function($model){
            if(empty($model->id)){
                $model->id = Str::uuid();
                
            }
        });
    }

    public function role(){
        return $this->belongsTo('App\role');
    }

    public function otp_code(){
        return $this->hasOne('App\OtpCode');
    }
}
